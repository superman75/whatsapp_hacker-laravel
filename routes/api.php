<?php

use Illuminate\Http\Request;
use App\Message;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/welcome', function () {
    return 'WOW great!!!!'; 
});
// Route::get('messages', function() {
//     // If the Content-Type and Accept headers are set to 'application/json', 
//     // this will return a JSON structure. This will be cleaned up later.
    
//     return Message::all();
// });
// Route::put('messages/{id}', function(Request $request, $id) {
//     $message = Message::findOrFail($id);
//     $message->update($request->all());

//     return $article;
// });

// Route::delete('messages/{id}', function($id) {
//     Message::find($id)->delete();

//     return 204;
// });


Route::get('messages', 'MessageController@index');
Route::get('messages/{message}', 'MessageController@show');
Route::post('messages', 'MessageController@store');
// Route::put('messages/{message}', 'MessageController@update');
// Route::delete('messages/{message}', 'MessageController@delete');