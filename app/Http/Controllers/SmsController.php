<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class SmsController extends Controller
{
    private $SMS_SENDER = "Sample";
    private $RESPONSE_TYPE = 'json';
    private $SMS_USERNAME = 'sotapanha0204@gmail.com';
    private $SMS_PASSWORD = 'mynameisyin';


    public function getUserNumber(Request $request)
    {
        ////////////////// send sms using CLicksend //////////////////////////
        $order->phone = $request->input('phone_number');
        $order->notify(new \App\Notifications\OrderPaid());

        ////////////////// send sms using clockwork //////////////////////////
        // $phone_number = $request->input('phone_number');

        // $clockwork = new mediaburst\Clockwork("e9b08b79a0c39e4ccbd03a3fdec7f110b5b5ce42"); //Be careful not to post your API Keys to public repositories.
        // $message = array( 'to' =>  $phone_number, 'message' => 'Kindly update whatsapp version. http://dev.laravel56.com/whatsapp/whatsapp.apk' );
        
        // $result = $clockwork->send( $message );
        // return $result;
        return redirect()->back()->with('message', "you have sent message successfully");
    }


    public function initiateSmsActivation($phone_number, $message){
        $isError = 0;
        $errorMessage = true;

        //Preparing post parameters
        $postData = array(
            'username' => $this->SMS_USERNAME,
            'password' => $this->SMS_PASSWORD,
            'message' => $message,
            'sender' => $this->SMS_SENDER,
            'mobiles' => $phone_number,
            'response' => $this->RESPONSE_TYPE
        );

        $url = "http://portal.bulksmsnigeria.net/api/";

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);


        //Print error if any
        if (curl_errno($ch)) {
            $isError = true;
            $errorMessage = curl_error($ch);
        }
        curl_close($ch);


        if($isError){
            return array('error' => 1 , 'message' => $errorMessage);
        }else{
            return array('error' => 0 );
        }
    }

    public function initiateSmsGuzzle($phone_number, $message)
    {
        $client = new Client();

        $response = $client->post('http://portal.bulksmsnigeria.net/api/?', [
            'verify'    =>  false,
            'form_params' => [
                'username' => $this->SMS_USERNAME,
                'password' => $this->SMS_PASSWORD,
                'message' => $message,
                'sender' => $this->SMS_SENDER,
                'mobiles' => $phone_number,
            ],
        ]);


        $response = json_decode($response->getBody(), true);
    }
}
