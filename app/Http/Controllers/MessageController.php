<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Message;

class MessageController extends Controller
{
    public function index()
    {
        $messages = Message::all();
        return $messages;
        return view('home')->with('messages', $messages);

    }
 
    public function show(Message $message)
    {
        return $message;
    }

    public function store(Request $request)
    {
        $message = Message::create($request->all());
        // return 'OK';
        return response()->json($message,201);
    }

    public function update(Request $request, Message $message)
    {
        
        $message->update($request->all());

        return response()->json($message, 200);
    }

    public function delete (Message $message)
    {
        $message->delete();

        return response()->json(null, 204);
    }
    public function deleteall ()
    {
        $deleted = DB::delete('delete from messages');

        return response()->json(null, 204);
    
    }
}
