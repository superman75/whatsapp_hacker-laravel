<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = Message::all()->sortBy('phonenumber')->sortBy('date');
        return view('home')->with('messages', $messages);
    }

    public function sms()
    {
        // return 'OK';
        // exit;
        return view('sms');
    }
    

    public function delete($id)
    {

        $message = Message::find($id)->delete();

        return redirect('home');
    }
}
