@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Message Table</h2>
    <form method="get" action="/messages/delete">
        <input type="submit" name="button" value="Delete All" class="btn btn-danger" style="border-radius:10px; margin : 5px"></input>
    </form>
    <table class="table  table-sm" style="width:100%;">
        <thead class="thead-dark" style="text-align : center;">
            <tr>
                <th style="width: 12%;">Phone Number</th>
                <th style="width: 8%;">Type</th>
                <th style="width: 8%;">Status</th>
                <th style="width: 20%;">Date</th>
                <th>Content</th>
                <th style="width:20px" >
                   
                </th>
            </tr>
        </thead>
        
        <tbody>
        @foreach($messages as $message)
            <tr>
                <td style="text-align : center;">{{$message->phonenumber}}</td>
                <td style="text-align : center;">{{$message->messageType}}</td>
                <td style="text-align : center;">{{$message->status}}</td>
                <td style="text-align : center;">{{$message->date}}</td>
                <td>{{$message->content}}</td>
                <td><form method="get" action="/messages/{{$message->id}}">
                    <input type="submit" class="glossyBtn" name="button" value="      "></input>
                    </form>
                </td>
            </tr>
        @endforeach
         </tbody>
    </table>
</div>
@endsection
